use ratatui::{
    prelude::*,
    widgets::{block::*, *},
};

use crate::app::App;

/// Renders the user interface widgets.
pub fn render(app: &mut App, frame: &mut Frame) {
    // This is where you add new widgets.
    // See the following resources:
    // - https://docs.rs/ratatui/latest/ratatui/widgets/index.html
    // - https://github.com/ratatui-org/ratatui/tree/master/examples

    let boot_options = ["Standard", "Update"];

    let outer_rect = frame.size().inner(&Margin {
        vertical: 1,
        horizontal: 1,
    });
    let inner_rect = outer_rect.inner(&Margin {
        vertical: frame.size().height/5,
        horizontal: frame.size().width/5,
    });

    // This widget is just a frame around the edges.
    frame.render_widget(
        Block::bordered()
            .title(Title::from(
                "Akoziland | Boot Mode Selection"
                    .yellow()
                    .bold()
                    .not_dim()
                )
            )
            .border_style(Style::default().fg(Color::Green).dim()),
        outer_rect
    );
    // This widget serves as the list of boot options
    frame.render_widget(
        List::new(boot_options)
            .block(Block::bordered()
                .title(Title::from("Boot Entries".white().bold()))
                .border_style(Color::DarkGray)
            ),
        inner_rect
    );
}
